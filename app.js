// app.js
const express = require('express');
const app = express();
const port = 3000; // Set the port number (e.g., 3000)

// Set the view engine to EJS
app.set('view engine', 'ejs');

// Define a route for the home page
app.get('/', (req, res) => {
  // Render the "index.ejs" template
  res.render('index');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

